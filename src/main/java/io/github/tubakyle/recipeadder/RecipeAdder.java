package io.github.tubakyle.recipeadder;

/**
 * Created by Kyle on 7/15/2014.
 */
/* TODO
-Add all monster eggs, potions
-make config changeable by command
-make above command reload config ^^
 */
/* UPDATES
Name of server in /ver
Even more chat colors.
sponge
Added 'RecipeAdder.user' permission.
Chain Armor
 */

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.logging.Logger;

public class RecipeAdder extends JavaPlugin {
    private static final String ver = "2.0";
    Logger log = Logger.getLogger("Minecraft");
    public void onEnable() {
        getConfig().options().copyDefaults(true);
        saveConfig();

        if (isEnabled("EndPortalFrame")) {
            ShapedRecipe frame = new ShapedRecipe(new ItemStack(Material.ENDER_PORTAL_FRAME, 1));
            frame.shape("DED", "OEO", "SSS");
            frame.setIngredient('D', Material.DIAMOND);
            frame.setIngredient('E', Material.EYE_OF_ENDER);
            frame.setIngredient('O', Material.OBSIDIAN);
            frame.setIngredient('S', Material.SANDSTONE);
            getServer().addRecipe(frame);
        }

        if (isEnabled("DiamondOre")) {
            ShapedRecipe diamondore = new ShapedRecipe(new ItemStack(Material.DIAMOND_ORE, 1));
            diamondore.shape("QQQ", "QDQ", "QCQ");
            diamondore.setIngredient('D', Material.DIAMOND);
            diamondore.setIngredient('C', Material.COBBLESTONE);
            getServer().addRecipe(diamondore);
        }

        if (isEnabled("GoldOre")) {
            ShapedRecipe goldore = new ShapedRecipe(new ItemStack(Material.GOLD_ORE, 1));
            goldore.shape("QQQ", "QGQ", "QCQ");
            goldore.setIngredient('G', Material.GOLD_INGOT);
            goldore.setIngredient('C', Material.COBBLESTONE);
            getServer().addRecipe(goldore);
        }

        if (isEnabled("RedstoneOre")) {
            ShapedRecipe redstoneore = new ShapedRecipe(new ItemStack(Material.REDSTONE_ORE, 1));
            redstoneore.shape("QQQ", "RRR", "RCR");
            redstoneore.setIngredient('C', Material.COBBLESTONE);
            redstoneore.setIngredient('R', Material.REDSTONE);
            getServer().addRecipe(redstoneore);
        }

        if (isEnabled("CoalOre" )) {
            ShapedRecipe coalore = new ShapedRecipe(new ItemStack(Material.COAL_ORE, 1));
            coalore.shape("QQQ", "QBQ", "QCQ");
            coalore.setIngredient('C', Material.COBBLESTONE);
            coalore.setIngredient('B', Material.COAL);
            getServer().addRecipe(coalore);
        }

        if (isEnabled("IronOre")) {
            ShapedRecipe ironore = new ShapedRecipe(new ItemStack(Material.IRON_ORE, 1));
            ironore.shape("QQQ", "QIQ", "QCQ");
            ironore.setIngredient('C', Material.COBBLESTONE);
            ironore.setIngredient('I', Material.IRON_INGOT);
            getServer().addRecipe(ironore);
        }

        if (isEnabled("LapisOre")) {
            ShapedRecipe lapisore = new ShapedRecipe(new ItemStack(Material.LAPIS_ORE, 1));
            lapisore.shape("QQQ", "LLL", "LCL");
            lapisore.setIngredient('C', Material.COBBLESTONE);
            lapisore.setIngredient('L', Material.INK_SACK, (short) 4);
            getServer().addRecipe(lapisore);
        }

        if (isEnabled("MobSpawner")) {
            ShapedRecipe mobspawner = new ShapedRecipe(new ItemStack(Material.MOB_SPAWNER, 1));
            mobspawner.shape("III", "IEI", "ITI");
            mobspawner.setIngredient('I', Material.IRON_FENCE);
            mobspawner.setIngredient('E', Material.EGG);
            mobspawner.setIngredient('T', Material.TORCH);
            getServer().addRecipe(mobspawner);
        }

        if (isEnabled("CobWeb")) {
            ShapedRecipe cobwebs = new ShapedRecipe(new ItemStack(Material.WEB, 4));
            cobwebs.shape("SSS", "SSS", "SSS");
            cobwebs.setIngredient('S', Material.STRING);
            getServer().addRecipe(cobwebs);
        }

        if (isEnabled("CreeperEgg")) {
            ShapedRecipe creeperegg = new ShapedRecipe(new ItemStack(Material.MONSTER_EGG, 1, (short) 50));
            creeperegg.shape("GGG", "GEG", "GGG");
            creeperegg.setIngredient('G', Material.SULPHUR);
            creeperegg.setIngredient('E', Material.EGG);
            getServer().addRecipe(creeperegg);
        }

        if (isEnabled("SkeletonEgg")) {
            ShapedRecipe skeletonegg = new ShapedRecipe(new ItemStack(Material.MONSTER_EGG, 1, (short) 51));
            skeletonegg.shape("BBB", "ECA", "BBB");
            skeletonegg.setIngredient('B', Material.BONE);
            skeletonegg.setIngredient('A', Material.ARROW);
            skeletonegg.setIngredient('E', Material.EGG);
            skeletonegg.setIngredient('C', Material.BOW);
            getServer().addRecipe(skeletonegg);
        }

        if (isEnabled("SpiderEgg")) {
            ShapedRecipe spideregg = new ShapedRecipe(new ItemStack(Material.MONSTER_EGG, 1, (short) 52));
            spideregg.shape("SSS", "SES", "SSS");
            spideregg.setIngredient('S', Material.STRING);
            spideregg.setIngredient('E', Material.EGG);
            getServer().addRecipe(spideregg);
        }

        if (isEnabled("ZombieEgg")) {
            ShapedRecipe zombieegg = new ShapedRecipe(new ItemStack(Material.MONSTER_EGG, 1, (short) 54));
            zombieegg.shape("FFF", "FEF", "FFF");
            zombieegg.setIngredient('F', Material.ROTTEN_FLESH);
            zombieegg.setIngredient('E', Material.EGG);
            getServer().addRecipe(zombieegg);
        }

        if (isEnabled("SlimeEgg")) {
            ShapedRecipe slimeegg = new ShapedRecipe(new ItemStack(Material.MONSTER_EGG, 1, (short) 55));
            slimeegg.shape("QQQ", "QSQ", "QEQ");
            slimeegg.setIngredient('S', Material.SLIME_BALL);
            slimeegg.setIngredient('E', Material.EGG);
            getServer().addRecipe(slimeegg);
        }

        if (isEnabled("GhastEgg")) {
            ShapedRecipe GhastEgg = new ShapedRecipe(new ItemStack(Material.MONSTER_EGG, 1, (short) 56));
            GhastEgg.shape("GFG", "TET", "GFG");
            GhastEgg.setIngredient('G', Material.SULPHUR);
            GhastEgg.setIngredient('F', Material.FIREBALL);
            GhastEgg.setIngredient('T', Material.GHAST_TEAR);
            GhastEgg.setIngredient('E', Material.EGG);
            getServer().addRecipe(GhastEgg);
        }

        if (isEnabled("ClayBrick")) {
            ShapedRecipe claybrick = new ShapedRecipe(new ItemStack(Material.CLAY_BRICK, 16));
            claybrick.shape("CCC", "CCC", "CCC");
            claybrick.setIngredient('C', Material.COBBLESTONE);
            getServer().addRecipe(claybrick);
        }

        if (isEnabled("Saddle")) {
            ShapedRecipe saddle = new ShapedRecipe(new ItemStack(Material.SADDLE, 2));
            saddle.shape("LLL", "LLL", "QQS");
            saddle.setIngredient('L', Material.LEATHER);
            saddle.setIngredient('S', Material.STRING);
            getServer().addRecipe(saddle);
        }

        if (isEnabled("Sponge")) {
            ShapedRecipe Sponge = new ShapedRecipe(new ItemStack(Material.SPONGE, 2));
            Sponge.shape("DDD", "DGD", "DDD");
            Sponge.setIngredient('D', Material.INK_SACK, (short) 11);
            Sponge.setIngredient('G', Material.GLOWSTONE_DUST);
            getServer().addRecipe(Sponge);
        }

        if (isEnabled("ChainHelmet")) {
            ShapedRecipe ChainHelmet = new ShapedRecipe(new ItemStack(Material.CHAINMAIL_HELMET, 1));
            ChainHelmet.shape("III", "IQI", "QQQ");
            ChainHelmet.setIngredient('I', Material.IRON_FENCE);
            getServer().addRecipe(ChainHelmet);
        }

        if (isEnabled("ChainChestPlate")) {
            ShapedRecipe ChainHelmet = new ShapedRecipe(new ItemStack(Material.CHAINMAIL_CHESTPLATE, 1));
            ChainHelmet.shape("III", "IQI", "QQQ");
            ChainHelmet.setIngredient('I', Material.IRON_FENCE);
            getServer().addRecipe(ChainHelmet);
        }

        if (isEnabled("ChainLeggings")) {
            ShapedRecipe ChainLeggings = new ShapedRecipe(new ItemStack(Material.CHAINMAIL_LEGGINGS, 1));
            ChainLeggings.shape("III", "IQI", "IQI");
            ChainLeggings.setIngredient('I', Material.IRON_FENCE);
            getServer().addRecipe(ChainLeggings);
        }

        if (isEnabled("ChainBoots")) {
            ShapedRecipe ChainBoots = new ShapedRecipe(new ItemStack(Material.CHAINMAIL_BOOTS, 1));
            ChainBoots.shape("QQQ", "IQI", "IQI");
            ChainBoots.setIngredient('I', Material.IRON_FENCE);
            getServer().addRecipe(ChainBoots);
        }

        log.info("[RecipeAdder] RecipeAdder v" + ver + " by kps1796 is done loading!");
    }

    public void onDisable() {
        log.info("[RecipeAdder] RecipeAdder v" + ver + " by kps1796 is now done unloading!");
    }

    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (cmd.getName().equalsIgnoreCase("ra")){
            if (args.length > 0) {
                if (args[0].equalsIgnoreCase("EndPortalFrame")) {
                    sendRecipeInfo(sender, "n", "End Portal Frame Block", "s", "Diamond (D), Eye of Ender (E), Obsidian (O), and Sandstone (S)", Boolean.FALSE, "DED", "OEO", "SSS", "One", "1", "", "EndPortalFrame");
                    return true;
                } else if (args[0].equalsIgnoreCase("DiamondOre")) {
                    sendRecipeInfo(sender, "", "Diamond Ore Block", "s", "Diamond (D), and Cobblestone (C)", Boolean.TRUE, "---", "-D-", "-C-", "One", "1", "", "DiamondOre");
                    return true;
                } else if (args[0].equalsIgnoreCase("GoldOre")) {
                    sendRecipeInfo(sender, "", "Gold Ore Block", "s", "Gold Ingot (G), and Cobblestone (C)", Boolean.TRUE, "---", "-G-", "-C-", "One", "1", "", "GoldOre");
                    return true;
                } else if (args[0].equalsIgnoreCase("RedstoneOre")) {
                    sendRecipeInfo(sender, "", "Redstone Ore Block", "s", "Redstone (R), and Cobblestone (C)", Boolean.TRUE, "---", "RRR", "RCR", "One", "1", "", "RedstoneOre");
                    return true;
                } else if (args[0].equalsIgnoreCase("CoalOre")) {
                    sendRecipeInfo(sender, "", "Coal Ore Block", "s", "Coal (B), and Cobblestone (C)", Boolean.TRUE, "---", "-B-", "-C-", "One", "1", "", "CoalOre");
                    return true;
                } else if (args[0].equalsIgnoreCase("IronOre")) {
                    sendRecipeInfo(sender, "n", "Iron Ore Block", "s", "Iron Ingot (I), and Cobblestone (C)", Boolean.TRUE, "---", "-I-", "-C-", "One", "1", "", "IronOre");
                    return true;
                } else if (args[0].equalsIgnoreCase("LapisOre")) {
                    sendRecipeInfo(sender, "", "Lapis Lazuli Ore Block", "s", "Lapis Lazuli (L), and Cobblestone (C)", Boolean.TRUE, "---", "LLL", "LCL", "One", "1", "", "LapisOre");
                    return true;
                } else if (args[0].equalsIgnoreCase("MobSpawner")) {
                    sendRecipeInfo(sender, "", "Mob Spawner", "s", "Iron Bars (I), Torch (T) and Egg (E)", Boolean.FALSE, "III", "IEI", "ITI", "One", "1", "", "MobSpawner");
                    return true;
                } else if (args[0].equalsIgnoreCase("CobWeb")) {
                    sendRecipeInfo(sender, "", "Cobweb", "", "String (S)", Boolean.FALSE, "SSS", "SSS", "SSS", "Four", "4", "s", "CobWeb");
                    return true;
                } else if (args[0].equalsIgnoreCase("CreeperEgg")) {
                    sendRecipeInfo(sender, "", "Creeper Egg", "s", "Gunpowder (G), and Egg (E)", Boolean.FALSE, "GGG", "GEG", "GGG", "One", "1", "", "CreeperEgg");
                    return true;
                } else if (args[0].equalsIgnoreCase("SkeletonEgg")) {
                    sendRecipeInfo(sender, "", "Skeleton Egg", "s", "Arrow (A), Bow (C), Bone (B), and Egg (E)", Boolean.FALSE, "BBB", "ECA", "BBB", "One", "1", "", "SkeletonEgg");
                    return true;
                } else if (args[0].equalsIgnoreCase("SpiderEgg")) {
                    sendRecipeInfo(sender, "", "Spider Egg", "s", "String (S), and Egg (E)", Boolean.FALSE, "SSS", "SES", "SSS", "One", "1", "", "SpiderEgg");
                    return true;
                } else if (args[0].equalsIgnoreCase("ZombieEgg")) {
                    sendRecipeInfo(sender, "", "Zombie Egg", "s", "Rotten Flesh (F), and Egg (E)", Boolean.FALSE, "RRR", "RER", "RRR", "One", "1", "", "ZombieEgg");
                    return true;
                } else if (args[0].equalsIgnoreCase("SlimeEgg")) {
                    sendRecipeInfo(sender, "", "Slime Egg", "s", "Slimeball (S), and Egg (E)", Boolean.TRUE, "---", "-S-", "-E-", "One", "1", "", "SlimeEgg");
                    return true;
                } else if (args[0].equalsIgnoreCase("GhastEgg")) {
                    sendRecipeInfo(sender, "", "Ghast Egg", "s", "Gunpowder (G), Fire Charge (F), Ghast Tear (T), and Egg (E)", Boolean.FALSE, "GFG", "GFG", "-E-", "One", "1", "", "GhastEgg");
                    return true;
                } else if (args[0].equalsIgnoreCase("ClayBrick")) {
                    sendRecipeInfo(sender, "", "Clay Brick", "", "Cobblestone (C)", Boolean.FALSE, "CCC", "CCC", "CCC", "Sixteen", "16", "s", "ClayBrick");
                    return true;
                } else if (args[0].equalsIgnoreCase("Saddle")) {
                    sendRecipeInfo(sender, "", "Saddle", "s", "Leather (L), and String (S)", Boolean.TRUE, "LLL", "LLL", "--S", "Two", "2", "s", "Saddle");
                    return true;
                } else if (args[0].equalsIgnoreCase("Sponge")) {
                    sendRecipeInfo(sender, "", "Sponge Block", "s", "Dandelion Yellow Dye (D), and Glowstone Dust (G)", Boolean.FALSE, "DDD", "DGD", "DDD", "Two", "2", "s", "Sponge");
                    return true;
                } else if (args[0].equalsIgnoreCase("ChainHelmet")) {
                    sendRecipeInfo(sender, "", "Chainmail Helmet", "", "Iron Bars (I)", Boolean.TRUE, "III", "I-I", "---", "One", "1", "", "ChainHelmet");
                    return true;
                } else if (args[0].equalsIgnoreCase("ChainChestPlate")) {
                    sendRecipeInfo(sender, "", "Chainmail Chestplate", "", "Iron Bars (I)", Boolean.TRUE, "I-I", "III", "III", "One", "1", "", "ChainChestPlate");
                    return true;
                } else if (args[0].equalsIgnoreCase("ChainLeggings")) {
                    sendRecipeInfo(sender, "", "Chainmail Leggings", "", "Iron Bars (I)", Boolean.TRUE, "III", "I-I", "I-I", "One", "1", "", "ChainLeggings");
                    return true;
                } else if (args[0].equalsIgnoreCase("ChainBoots")) {
                    sendRecipeInfo(sender, "", "Chainmail Boots", "", "Iron Bars (I)", Boolean.TRUE, "---", "I-I", "I-I", "One", "1", "", "ChainBoots");
                    return true;
                } else if (args[0].equalsIgnoreCase("list")) {
                    if (sender.hasPermission("RecipeAdder.list")) {
                        sender.sendMessage(ChatColor.GOLD + "Craftable Items: " + ChatColor.AQUA
                                + "EndPortalFrame"
                                + ChatColor.GOLD + ", " + ChatColor.AQUA
                                + "DiamondOre"
                                + ChatColor.GOLD + ", " + ChatColor.AQUA
                                + "GoldOre"
                                + ChatColor.GOLD + ", " + ChatColor.AQUA
                                + "RedstoneOre"
                                + ChatColor.GOLD + ", " + ChatColor.AQUA
                                + "CoalOre"
                                + ChatColor.GOLD + ", " + ChatColor.AQUA
                                + "IronOre"
                                + ChatColor.GOLD + ", " + ChatColor.AQUA
                                + "LapisOre"
                                + ChatColor.GOLD + ", " + ChatColor.AQUA
                                + "MobSpawner"
                                + ChatColor.GOLD + ", " + ChatColor.AQUA
                                + "CobWeb"
                                + ChatColor.GOLD + ", " + ChatColor.AQUA
                                + "CreeperEgg"
                                + ChatColor.GOLD + ", " + ChatColor.AQUA
                                + "SkeletonEgg"
                                + ChatColor.GOLD + ", " + ChatColor.AQUA
                                + "SpiderEgg"
                                + ChatColor.GOLD + ", " + ChatColor.AQUA
                                + "ZombieEgg"
                                + ChatColor.GOLD + ", " + ChatColor.AQUA
                                + "SlimeEgg"
                                + ChatColor.GOLD + ", " + ChatColor.AQUA
                                + "GhastEgg"
                                + ChatColor.GOLD + ", " + ChatColor.AQUA
                                + "ClayBrick"
                                + ChatColor.GOLD + ", " + ChatColor.AQUA
                                + "Saddle"
                                + ChatColor.GOLD + ", " + ChatColor.AQUA
                                + "Sponge"
                                + ChatColor.GOLD + ", " + ChatColor.AQUA
                                + "ChainHelmet"
                                + ChatColor.GOLD + ", " + ChatColor.AQUA
                                + "ChainChestPlate"
                                + ChatColor.GOLD + ", " + ChatColor.AQUA
                                + "ChainLeggings"
                                + ChatColor.GOLD + ", and " + ChatColor.AQUA
                                + "ChainBoots"
                                + ChatColor.GOLD + ".");
                    } else {
                        permError(sender, "RecipeAdder.list");
                    }
                    return true;
                } else if(args[0].equalsIgnoreCase("help")){
                    sendHelp(sender);
                    return true;
                } else if (args[0].equalsIgnoreCase("ver") || args[0].equalsIgnoreCase("version")) {
                    if (sender.hasPermission("RecipeAdder.version")) {
                        sender.sendMessage(ChatColor.GOLD + "The currently installed version of RecipeAdder is v" + ver + ".");
                        sender.sendMessage(ChatColor.BLUE + "This plugin was made by kps1796.");
                    } else {
                        permError(sender, "RecipeAdder.version");
                    }
                    return true;
                } else if (args[0].equalsIgnoreCase("reload")) {
                    if (sender.hasPermission("RecipeAdder.reload")) {
                        log.info("[RecipeAdder] " + sender.getName() + " reloaded the config...");
                        reloadConfig();
                        log.info("[RecipeAdder] Config done reloading.");
                        sender.sendMessage(ChatColor.GOLD + "RecipeAdder config has been succesfully reloaded.");
                    } else {
                        permError(sender, "RecipeAdder.reload");
                    }
                    return true;
                } else if (args.length > 1) {
                    sender.sendMessage(ChatColor.RED + "Invalid amount of arguments.");
                    sendHelp(sender);
                    return true;
                } else {
                    sender.sendMessage(ChatColor.RED + "Invalid item/argument.");
                    sendHelp(sender);
                    return true;
                }

            } else if (args.length==0){
                sendHelp(sender);
            } else {
                sendHelp(sender);
                return true;
            }
        }
        return false;
    }

    public void sendRecipeInfo(CommandSender sender, String an, String blocktype, String materials, String materialslist, Boolean q, String R1, String R2, String R3, String numword, String num, String blocks, String configname) {
        if (sender.hasPermission("RecipeAdder.help")) {
            if (isEnabled(configname)) {
                sender.sendMessage("=====[ " + ChatColor.DARK_GREEN + "RecipeAdder" + ChatColor.GREEN + " Recipe " + ChatColor.WHITE + "]=====");
                sender.sendMessage("To craft a" + an + " " + ChatColor.BLUE + blocktype + ChatColor.WHITE + ", use the following material" + materials + ChatColor.GRAY + ": " + ChatColor.GREEN +  materialslist + ChatColor.GRAY + ".");
                if (q) {
                    sender.sendMessage("('" + ChatColor.GREEN + "-" + ChatColor.WHITE + "' is blank.)");
                }
                sender.sendMessage("Here is the layout for a 3x3 crafting grid" + ChatColor.GRAY + ":");
                sender.sendMessage(ChatColor.GREEN + R1);
                sender.sendMessage(ChatColor.GREEN + R2);
                sender.sendMessage(ChatColor.GREEN + R3);
                sender.sendMessage("You will receive " + ChatColor.LIGHT_PURPLE + numword + ChatColor.AQUA + " (" + ChatColor.LIGHT_PURPLE +  num + ChatColor.AQUA +  ") " + ChatColor.BLUE + blocktype + blocks + ChatColor.WHITE + ".");
                sender.sendMessage("=============================");
            } else {
                disabled(an, configname, sender);
            }
        } else {
            permError(sender, "RecipeAdder.help");
        }
    }
    public boolean isEnabled(String recipe) {
        if (getConfig().getString(recipe) == "true") {
            return true;
        } else {
            return false;
        }
    }
    public void disabled(String an, String recipe, CommandSender sender) {
        sender.sendMessage("Sorry, but the crafting recipe for a" + an + " " + recipe + " is disabled on this server.");
    }
    public void permError(CommandSender sender, String perm) {
        sender.sendMessage("You do not have the required permissions node. " + ChatColor.AQUA + "(" + ChatColor.GOLD +  perm + ChatColor.AQUA + ")");
        sender.sendMessage(ChatColor.RED + "If you feel like you have received this message in error, please contact your server administrator.");
    }
    public void sendHelp(CommandSender sender) {
        if (sender.hasPermission("RecipeAdder.help")) {
            sender.sendMessage("=====[ " + ChatColor.DARK_GREEN + "RecipeAdder" + ChatColor.GOLD + " Help " + ChatColor.WHITE + "]=====");
            sender.sendMessage(ChatColor.GOLD + "/ra help" + ChatColor.GREEN + " - " + ChatColor.WHITE + "shows this help section.");
            sender.sendMessage(ChatColor.GOLD + "/ra " + ChatColor.AQUA + "(" + ChatColor.GOLD + "ver " + ChatColor.AQUA + "/" + ChatColor.GOLD + " version" + ChatColor.AQUA + ")" + ChatColor.GREEN + " -" + ChatColor.WHITE + " shows the current version of RecipeAdder.");
            sender.sendMessage(ChatColor.GOLD + "/ra list" + ChatColor.GREEN + " -" + ChatColor.WHITE + " shows a list of craftable items.");
            sender.sendMessage(ChatColor.GOLD + "/ra " + ChatColor.AQUA + "(" + ChatColor.GOLD + "ItemNameWithNoSpaces" + ChatColor.AQUA + ")" + ChatColor.GREEN + " -" + ChatColor.WHITE + " shows how to craft an item.");
            sender.sendMessage(ChatColor.GOLD + "/ra reload" + ChatColor.GREEN + " -" + ChatColor.WHITE + " reloads the RecipeAdder config.");
            sender.sendMessage(ChatColor.BLUE + "This plugin was made by kps1796.");
            sender.sendMessage("===========================");
        } else {
            permError(sender, "RecipeAdder.help");
        }
    }
}
