# RecipeAdder
Adds crafting recipes for non-craftable items in minecraft.

## Current Recipe List
 * End Portal Frame 
 * Diamond Ore 
 * Gold Ore 
 * Redstone Ore 
 * Coal Ore 
 * Iron Ore 
 * Lapis Lazuli Ore 
 * Mob Spawner 
 * Cobweb 
 * Creeper Egg 
 * Skeleton Egg 
 * Spider Egg 
 * ZombieEgg 
 * Slime Egg
 * Ghast Egg 
 * Clay Brick 
 * Saddle
 * Sponge
 * Chainmail Armor (Helmet, chestplate, leggings, and boots)

## Requests
Click tickets above and create a new ticket. (a.k.a 'issue')
<br> Request format:
<pre>
[Ingredient 1] (A) 
[Ingredient 2] (B) 
[Ingredient 3] (C) 
[Ingredient 4] (D) 

A B A
B B D
C C B

Makes: [QUANTITY] [ITEM] 
</pre>

## Commands
 * /ra 
 * /ra help 
 * /ra list 
 * /ra ver (or /ra version) 
 * /ra reload
 * /ra [ItemNameWithNoSpaces]

## Permissions

RecipeAdder.*:
 
    description: Gives access to all RecipeAdder commands.
     
RecipeAdder.user:

    description: Gives access to all RecipeAdder commands except /ra reload.
    
RecipeAdder.help:
 
    description: Allows use of the help command for RecipeAdder.
     
    default: op
     
RecipeAdder.list:
 
    description: Allows viewing of the RecipeAdder recipe list.
     
    default: op
     
RecipeAdder.version:
 
    description: Allows viewing of the currently installed version of RecipeAdder.
     
    default: op
     
RecipeAdder.reload:
 
    description: Gives the ability to reload the RecipeAdder config.
     
    default: op